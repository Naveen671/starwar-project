import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TabsComponent } from "./tabs/tabs.component";
import { ListComponent } from "./list/list.component";


const routes = [
  { path:'character', component: TabsComponent, children: [
      { path: '', redirectTo: '/character/all', pathMatch: 'full'},
      { path:':side', component: ListComponent}
    ] },
  //when ever you want to only load the required componets and ha\ving a seperate module you need to use this child loadinf to implement lazyloading
  { path:'add-character', loadChildren:'./create-character/create-character.module#CreateCharacterModule' },
  // catching exception through this
  { path: '**', redirectTo: '/character'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
