import {Component, Inject, OnInit} from '@angular/core';
import { starwarCharacter } from "../star-war-character.service";

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css'],
  // providers: [starwarCharacter]
})
export class TabsComponent implements OnInit {

  characters = [];
  chosenTab = 'all';

  constructor() {
    // this.starwarCharacter = starwarCharacter;
  }

  ngOnInit() {
  }

  // onChose(side){
  //   this.chosenTab = side;
  // }
  //
  // getCharacters(){
  //   this.characters = this.starwarCharacter.getCharacters(this.chosenTab);
  //   return this.characters;
  // }

}
