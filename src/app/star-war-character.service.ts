import { Injectable } from "@angular/core";
import { LogServiceService } from "./log-service.service";
import { Subject } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";

@Injectable(
  {
    providedIn: 'root'
  }
)
export class starwarCharacter {

  constructor(private logService: LogServiceService, private http: HttpClient) {
    this.http = http;
  }
  private characters = [
    { name: 'Naveen', side: ''},
    { name: 'Sirisha', side: ''}
  ];
  characterChanged = new Subject<void>();

  // for getting values from api and this first map is used to transform the data and then subscribe to the data that we need to send to the application.
  fetchCharacters (){
    this.http.get('https://swapi.dev/api/people/')
      .pipe(
        map((data) => {
          const extractedResults = data['results'];
          const newValue = extractedResults.map((char) =>{
            return {name: char.name, side: ''}
          })
          return newValue;
        })
      )
      .subscribe(
      data => {
        console.log(data);
        this.characters = data;
        this.characterChanged.next();
      }
    );
  }
ers
  // the filter method will help in looping throught the array and slice will provide dup object of the whole charact
  getCharacters(chosenTab){
    if (chosenTab === 'all'){
      return this.characters.slice();
    }
    return this.characters.filter((char) => {
      return char.side === chosenTab;
    })
  }

  onSideChosen(event){
    let pos = this.characters.findIndex((char) =>{
      return char.name === event.name;
    })
    this.characters[pos].side = event.side;
    this.characterChanged.next();
    this.logService.checkLog('Changed side of '+ event.name + ' to ' + event.side);
  }

  addCharacter(name, side){
    let pos = this.characters.findIndex((char)=>{
      return char.name === name;
    })
    if(pos != -1){
      return;
    }
    let newValues = {name: name, side:side};
    this.characters.push(newValues);
  }
}
