import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TabsComponent } from './tabs/tabs.component';
import { ListComponent } from './list/list.component';
import { ItemComponent } from './item/item.component';
import { starwarCharacter } from "./star-war-character.service";
import { LogServiceService } from "./log-service.service";
import { HeaderComponentComponent } from './header-component/header-component.component';
import { HttpClientModule } from "@angular/common/http";
import { CreateCharacterModule } from "./create-character/create-character.module";

@NgModule({
  declarations: [
    AppComponent,
    TabsComponent,
    ListComponent,
    ItemComponent,
    HeaderComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppRoutingModule,
    HttpClientModule,
    CreateCharacterModule
  ],
  // can be removed if you are using provideIn:root at the service you eant to use thorough in app
  providers: [
    // starwarCharacter,
    // LogServiceService
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
