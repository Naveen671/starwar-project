import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { starwarCharacter } from "../star-war-character.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit,OnDestroy {

  characters= [];
  loadedside = 'all';
  subscription: any;
  // @Output() sideAssigned = new EventEmitter<{name: string, side: string}>();
  constructor(private activatedRoute: ActivatedRoute, private starwarService: starwarCharacter) {
    this.activatedRoute = activatedRoute;
    this.starwarService = starwarService;
  }

  // onSideAssigned(event){
  // this.sideAssigned.emit(event);
  // }

  ngOnInit() {
    // used to define what is the active route of app and will route to the respective pages by determing the side selected
    this.activatedRoute.params.subscribe(
      (params) =>{
        this.characters = this.starwarService.getCharacters(params.side);
        this.loadedside = params.side;
      }
    )
    // this is created to change characters array when ever you change tabs and add new character
    this.subscription = this.starwarService.characterChanged.subscribe(
      () => {
        this.characters = this.starwarService.getCharacters(this.loadedside);
      }
    )
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
