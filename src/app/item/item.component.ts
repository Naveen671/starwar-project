import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import { starwarCharacter } from "../star-war-character.service";

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css'],
  // providers: [starwarCharacter]
})
export class ItemComponent implements OnInit {

  @Input() character;
  // @Output() sideAssigned = new EventEmitter<{name: string, side: string}>();
  constructor( private starwarCharacter: starwarCharacter) {
    // this.starwarCharacter = starwarCharacter;
  }

  ngOnInit() {
  }

  // will be used to decide on which side the user selected and will pass on the values that that the emitter is expecting
  onAssign(side){
    // this.character.side = side;
    // this.sideAssigned.emit({name: this.character.name, side:side});
    this.starwarCharacter.onSideChosen({name: this.character.name, side: side});
  }
}
