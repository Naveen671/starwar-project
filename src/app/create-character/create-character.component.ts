import { Component, OnInit } from '@angular/core';
import { starwarCharacter } from "../star-war-character.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-character',
  templateUrl: './create-character.component.html',
  styleUrls: ['./create-character.component.css']
})
export class CreateCharacterComponent implements OnInit {
  // Created for options display in  the select side for new character
  availableSides = [{display: 'None', value: ''}, {display: 'Light', value: 'light'}, {display: 'Dark', value: 'dark'}];

  constructor(private starwarCharacter: starwarCharacter, private activatedRoute: Router) {
    this.activatedRoute = activatedRoute;
  }

  ngOnInit() {
  }

  //will be used to submit the form and route to home page
  onSubmit(submittedForm){
    if(submittedForm.invalid){
      console.log('Please enter a valid Input');
      return;
    }
    console.log(submittedForm.value);
    this.starwarCharacter.addCharacter(submittedForm.value.name,submittedForm.value.side);
    this.activatedRoute.navigate(['/character'])
  }
}
