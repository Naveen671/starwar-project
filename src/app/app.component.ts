import { Component, OnInit } from '@angular/core';
import { starwarCharacter } from './star-war-character.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'starwar-project';

  constructor(private starwarService: starwarCharacter ) {
    this.starwarService = starwarService;
  }
  ngOnInit() {
    //initially load what ever the characters are from service
    this.starwarService.fetchCharacters();
  }
}
